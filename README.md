# ticketdemo

**Build application:**
mvn clean install

**Run application:**
mvn spring-boot:run

Notes:
This app is missing some production-ready-app components like logs, integration tests etc since i tried to keep it short due to nature of this assignment. Will be updated if necessary.