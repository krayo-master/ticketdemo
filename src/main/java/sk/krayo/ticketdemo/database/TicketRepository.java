package sk.krayo.ticketdemo.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long>
{
    Optional<Ticket> findFirstByOrderByCreatedAt();

    default void deleteTicketWithQueueOrderZero()
    {
        deleteByQueueOrder(0);
    }

    void deleteByQueueOrder(int queueOrder);

    @Modifying
    @Query("update TICKET t set t.queueOrder = t.queueOrder - 1")
    void shiftOrderByOne();
}
