package sk.krayo.ticketdemo.database;

import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.time.Instant;

@Entity(name = "TICKET")
public class Ticket
{
    public Ticket(@Nullable Long id, Instant createdAt, int queueOrder)
    {
        this.id = id;
        this.createdAt = createdAt;
        this.queueOrder = queueOrder;
    }

    public Ticket()
    {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Nullable
    private Long id;

    private Instant createdAt;

    private int queueOrder;

    @Nullable
    public Long getId()
    {
        return id;
    }

    public void setId(@Nullable Long id)
    {
        this.id = id;
    }

    public Instant getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt)
    {
        this.createdAt = createdAt;
    }

    public int getQueueOrder()
    {
        return queueOrder;
    }

    public void setQueueOrder(int queueOrder)
    {
        this.queueOrder = queueOrder;
    }
}
