package sk.krayo.ticketdemo.api.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import sk.krayo.ticketdemo.business.TicketNotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler
{
    @ExceptionHandler(TicketNotFoundException.class)
    public ResponseEntity<Error> handleTicketNotFoundException(TicketNotFoundException ticketNotFoundException)
    {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error(HttpStatus.NOT_FOUND.value(),
                ticketNotFoundException.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handleException(Exception exception)
    {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(HttpStatus.NOT_FOUND.value(),
                exception.getMessage()));
    }
}
