package sk.krayo.ticketdemo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.krayo.ticketdemo.business.TicketService;
import sk.krayo.ticketdemo.database.Ticket;

@RestController
public class TicketController
{
    private final TicketService ticketService;

    public TicketController(TicketService ticketService)
    {
        this.ticketService = ticketService;
    }

    @PostMapping("/ticket")
    public ResponseEntity<Ticket> generateNewTicket()
    {
        return ResponseEntity.ok(ticketService.generateNewTicket());
    }

    @GetMapping("/ticket")
    public ResponseEntity<Ticket> getCurrentlyActiveTicket()
    {
        return ResponseEntity.ok(ticketService.getCurrentlyActiveTicket());
    }

    @DeleteMapping("/ticket")
    public ResponseEntity<Void> deleteCurrentlyActiveTicket()
    {
        ticketService.deleteCurrentlyActiveTicket();

        return new ResponseEntity<>(HttpStatus.OK);
    }


}
