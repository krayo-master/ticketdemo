package sk.krayo.ticketdemo.business;

import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import sk.krayo.ticketdemo.database.Ticket;
import sk.krayo.ticketdemo.database.TicketRepository;

import java.time.Instant;

@Service
public class TicketService
{
    private final TicketRepository ticketRepository;

    public TicketService(TicketRepository ticketRepository)
    {
        this.ticketRepository = ticketRepository;
    }

    public Ticket generateNewTicket()
    {
        return ticketRepository.save(new Ticket(null, Instant.now(), (int) ticketRepository.count()));
    }


    public Ticket getCurrentlyActiveTicket()
    {
        return ticketRepository.findFirstByOrderByCreatedAt()
                .orElseThrow(() -> new TicketNotFoundException("No ticket found. Queue is empty."));
    }

    @Transactional
    public void deleteCurrentlyActiveTicket()
    {
        ticketRepository.deleteTicketWithQueueOrderZero();
        ticketRepository.shiftOrderByOne();
    }
}
