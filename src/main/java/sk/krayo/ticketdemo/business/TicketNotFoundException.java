package sk.krayo.ticketdemo.business;

/**
 * Thrown to indicate Ticket was not found.
 */
public class TicketNotFoundException extends RuntimeException
{
    public TicketNotFoundException(String message)
    {
        super(message);
    }
}
