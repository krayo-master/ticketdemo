package sk.krayo.ticketdemo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import sk.krayo.ticketdemo.business.TicketNotFoundException;
import sk.krayo.ticketdemo.business.TicketService;
import sk.krayo.ticketdemo.database.Ticket;
import sk.krayo.ticketdemo.database.TicketRepository;

import java.time.Instant;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TicketServiceTest
{
    private TicketService ticketService;
    private ArgumentCaptor<Ticket> ticketArgumentCaptor;
    private TicketRepository ticketRepository;

    @BeforeEach
    void setUp()
    {
        ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
        ticketRepository = mock(TicketRepository.class);
        ticketService = new TicketService(ticketRepository);
    }

    @Test
    void testGetCurrentlyActiveTicket()
    {
        Ticket ticket = mock(Ticket.class);
        when(ticketRepository.findFirstByOrderByCreatedAt()).thenReturn(Optional.of(ticket));
        assertThat(ticketService.getCurrentlyActiveTicket()).isEqualTo(ticket);
    }

    @Test
    void testGetCurrentlyActiveTicketFail()
    {
        when(ticketRepository.findFirstByOrderByCreatedAt()).thenReturn(Optional.empty());
        assertThatThrownBy(() -> ticketService.getCurrentlyActiveTicket())
                .isInstanceOf(TicketNotFoundException.class)
                .hasMessage("No ticket found. Queue is empty.");
    }

    @Test
    void testGenerateNewTicket()
    {
        ticketService.generateNewTicket();
        verify(ticketRepository).save(ticketArgumentCaptor.capture());
        Ticket capturedTicket = ticketArgumentCaptor.getValue();
        assertThat(capturedTicket.getId()).isEqualTo(null);
        assertThat(capturedTicket.getQueueOrder()).isEqualTo(0);
        assertThat(capturedTicket.getCreatedAt()).isBefore(Instant.now());
    }

    @Test
    void testDeleteCurrentlyActiveTicket()
    {
        ticketService.deleteCurrentlyActiveTicket();
        verify(ticketRepository).deleteTicketWithQueueOrderZero();
        verify(ticketRepository).shiftOrderByOne();
    }
}
